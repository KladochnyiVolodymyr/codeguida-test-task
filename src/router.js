import Vue from 'vue'
import Router from 'vue-router'
import Sweeties from './views/Sweeties.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'sweeties',
      component: Sweeties
    }
  ]
})
